
# IoC 控制反转
Inversion of Control  
它是一种概念/思想，在spring框架中表现为：由Spring容器进行对象的创建和依赖注入  
**正转：** 由程序员进行对象创建以及依赖注入，比如  
```
Student stu = new Student();
stu.setName("James");
```
控制反转实现方式，如
```
<bean id="stu" class="com.James.pojo.Student">  // 容器负责对象创建
    <property name="name" value="James">  // 依赖注入
</bean>
```

## 具体使用（基于xml）
1. 创建对象  
`resources/applicationContext.xml`
```
<bean>
<!-- 内容参考上方示例 -->
</bean>
```
*Java代码*
```
    @Test
    public void testStudentSpring() {
        // spring容器进行对象创建
        // spring容器创建时会自动创建其中的bean对象
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        Student stu = (Student) ac.getBean("stu");
        System.out.println(stu);
    }

```
2. 对象赋值  
&emsp;* 方式一：setter方法注入  
简单类型使用`value`属性，引用类型使用`ref`属性  
<sup>⚠️注意：</sup>使用该方式必需 1.对象提供无参构造方法 2.类有setter方法  
<sup>NOTE: </sup>啥时候会没有无参构造方法？**只** 手动给类重载了有参构造方法时  
&emsp;* 方式二：构造方法注入  
可以通过： ①构造方法的参数名 ②构造方法的参数下标 ③构造方法参数的默认顺序 几种常用方式实现注入  
*Java代码*
```
/*
构造方法参数名
*/
<bean id="stu" class="com.James.pojo.Student">
    <constructor-arg name="sid" value="001"></constructor-arg>
    <constructor-arg name="sname" value="James"></constructor-arg>
</bean>

/*
构造方法下标
*/
<bean id="stu" class="com.James.pojo.Student">
    <constructor-arg index="0" value="001"></constructor-arg>
    <constructor-arg index="1" value="James"></constructor-arg>
</bean>

/*
使用默认构造方法参数顺序
不需要 name/index 属性
直接使用 value/ref
*/
```

## 具体使用（基于注解）
也叫`DI`(Denpendency Injection)  
* 创建对象  
&emsp;- \@Component:  
创建任意对象  
&emsp;- \@Controller:  
用来创建控制器对象(Servlet)  
&emsp;- \@Service:  
用来创建业务逻辑层对象  
&emsp;- \@Repository:  
用来创建数据访问层对象(DAO)  
* 依赖注入  
&emsp;- \@Value:  
简单类型注入  
&emsp;- \@Autowired:  
引用类型注入，在Bean工厂中搜索同源类型注入  
<sup>同源：</sup>注入对象与被注入对象的关系为 ①相同类型对象 ②子父关系 ③实现类与接口的关系  
> 👉<sup>个人测试：</sup>Autowired为自动匹配Bean对象名
> 
> 比如：
> 
> Student类注入School类型对象school
>
> Bean工厂中自动生成了School和SubSchool两个类的对象
> 
> 两个对象没指定别名的，School对象默认对象名为"school"，会注入Student对象
> 
> 给SubSchool命名"school"(这时School对象需要手动命名，否则会因为Bean对象重名而报错)，则SubSchool独享会注入Student对象
>
> 2022/08/15 先找对应类，再找跟引用类型变量名匹配的Bean对象

&emsp;-  
\@Autowired  
\@Qualifier(BEAN_OBJECT_NAME):  
引用类型注入，在Bean工厂中搜索同名对象注入  
> 👉<sup>个人测试：两个需要连用才能实现指定Bean对象名
>
> 如果只用@Qualifier
>
> 则还是按照被注入对象的变量名来识别该注入哪个Bean对象
>
> 2022/08/15 \@Qualifier用来覆盖\@Autowired中自动调用的引用类型变量名

需要在Spring核心配置文件(applicationContext.xml)中开启包扫描  
```
<context:component-scan base-package="THE_PACKAGE_FULL_NAME_HERE"></context:component-scan>
包扫描除了可以逐条添加，也支持在一条中添加多个包
包之间使用 ','/' '/';' 隔开

当然，也可以指定高层级的包，比如 "com.james.crm"
但是这样子会影响Spring容器初始化速度(因为扫描范围大了)
```
创建的对象名称默认是类名的（小）驼峰命名形式  
若要指定对象名，可以使用`@Component("stu")`的形式手动指定  

### Spring配置文件拆分规则
当项目体量达到一定规模，此时便不适合将所有配置放在一个文件中  
因为配置文件会频繁地发生修改  
Spring配置文件此时可以按照 具体层级/项目功能 来进行拆分  
层级： controller/service/DAO  
功能： 用户管理/业务1/业务2/etc.  
拆分后你会获得一堆配置文件，新建一个xml配置文件，将其它配置文件导入做整合  
```
<!-- 单条导入 -->
<import resource="SUB_XML_PATH"></import>

<!--
    也支持多条导入
    使用通配符'*'即可
-->
```

## 补充知识
### XML方式自动注入
在bean标签中使用'autowire'属性来实现  
```
<bean ... autowire="byType/byName/constructor/etc."></bean>
```
