
# AOP
Aspect Orient Programming, 面向切面编程  
**切面**：公共的、通用的、重复的功能  

## 实现功能
通用功能通过动态代理，可以方便地织入各种类型的对象中  
在被代理对象的方法`执行前`、`执行后`、`抛出异常`、`Around`时实现其它功能  

# AspectJ框架

## 切入点(Pointcut)表达式
原型
```
execution(modifiers-pattern? ret-type-pattern declaring-type-pattern?name-pattern(param-pattern) throws-pattern?)

modifiers-pattern               访问权限
ret-type-pattern                返回值类型
declaring-type-pattern          包名、类名
name-pattern(param-pattern)     方法名(参数类型、参数个数)
throws-pattern                  抛出异常类型
* 带?表示可选项
```
可用通配符
```
*       任意多个字符
..      用在方法参数，表示任意多个参数
        用在包名中，表示当前包及其子包
+       用在类名后，表示当前类及其子类
        用在接口后，表示当前接口及其实现类
```

## 使用步骤
1. 创建业务接口  
2. 创建业务对象实现业务接口  
3. 创建切面对象  
4. 绑定业务对象和切面对象  
在spring配置文件中绑定两者  
```
<!-- 创建业务对象 -->
<bean id="..." class="..."></bean>
<!-- 创建切面对象 -->
<bean id="..." class="..."></bean>
<!-- 绑定两者 -->
<!--
        可以使用 proxy-target-class="true" 属性实现子类代理
        这时候bean对象的Class不是$Proxy，而是对应子类的$EnhancerBySpringCGLIB
        以及AspectJ支持 JDK动态代理 和 cglib动态代理 两种方式
-->
<aop:aspectj-autoproxy></aop:aspectj-autoproxy>
```

### 基于注解的实现
#### \@Before
前置通知  

#### \@AfterReturning
后置通知  

#### \@Around
环绕通知，功能最为强大  
一般处理事务使用该通知类型  

#### \@After
最终通知  
相当于Java语法中的`finally{}`语句块  

### 切入点表达式的别名
```
@Pointcut(value = "POINTCUT_PATTERN")
public void cutAlias() {}
```
届时其它AOP可以直接在其注解中使用该切入点表达式别名(即方法名)来引用该切入点  
