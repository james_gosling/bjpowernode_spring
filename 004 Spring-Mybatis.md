
#

# 事务传播特性
* REQUIRED  

* NOT_SUPPORTED  

* SUPPORTS  

* REQUIRES_NEW  

* NEVER  

* NESTED(不常用)  

# Spring事务管理
## 注解式
在类/方法上方使用`@Transactional`注解指定其事务管理方式  

## 声明式
在配置文件中配置事务切面、绑定切面和切入点  
```
<!--
    1. 导入mapper.xml
        <import resource="....xml">
    2. 添加包扫描
        <context:component-scan base-package="...">
    3. 添加事务管理器
        <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
            <property name="dateSource" ref="dataSource(在mapper中)">
    4. 配置事务切面
        <tx:advice id="myAdvice" transaction-manager="transactionManager">
            <tx:attributes>
                <tx:method name="*select*" read-only="true" />
                <tx:method name="*insert*" paragation="REQUIRED" />            
    5. 绑定切面和切入点
        <aop:config>
            <aop:pointcut id="myCut" expression="execution(* com.james.spring.*.*(..))">
            <aop:advisor advice-ref="myAdvice" pointcut-ref="myCut">
-->
```
